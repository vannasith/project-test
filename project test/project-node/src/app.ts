import express from 'express'
import cors from 'cors'
import bodyParser from 'body-parser'
import dotenv from 'dotenv'
import http from 'http'
import apollo from './graphql/apollo'
import Mongo from './plugins/mongoose'


const app: express.Application = express()
const monogoDB = new Mongo()
const jsonParser = bodyParser.json()
const urlencodedParser = bodyParser.urlencoded({ extended: false })
const port =  8083

// middleware
monogoDB.connect()
dotenv.config()
apollo.applyMiddleware({ app, path: '/graphql' })
app.use(express.static('public', { dotfiles: 'allow' })) // use public image
app.use(jsonParser)
app.use(urlencodedParser)
app.use(cors())


http.createServer(app).listen(port, () => {
    console.log('Server is runing',`http://localhost${apollo.graphqlPath}`)
})