import path from 'path';
import { fileLoader, mergeTypes } from 'merge-graphql-schemas'
 
const typeDefsArray = fileLoader(path.join(__dirname, './typeDefs'), { extensions: ['.ts'] })

export default mergeTypes(typeDefsArray, { all: true })