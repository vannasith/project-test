import { gql } from 'apollo-server-express'

const event = gql`
scalar Date

#client
type Event {
    _id: ID!
    title: String!
    date: Date!
    description: String 
    image: String
}

type Query {
    events: [Event]
}

type Mutation {
    createEvent(
        title: String
        date: Date
        description: String
        image: String
    ): String

    updateEvent(
        _id: ID
        title: String
        date: Date
        description: String
        image: String
    ): String

    deleteEvent(
        _id: ID!
    ): String
}



`
export default event 