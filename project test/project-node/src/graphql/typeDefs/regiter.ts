import { gql } from 'apollo-server-express'

const regiter = gql`
scalar Date

#client
type Regiter {
    _id: ID
    title: String,
    name: String,
    surname: String,
    type_of_business: String,
    company: String,
    province: String,
    position: String,
    mobile: String,
    email: String,
}
type Query {
    regiters: [Regiter]
}

 type Mutation {
    createRegiter(
    _id: ID
    title: String,
    name: String,
    surname: String,
    type_of_business: String,
    company: String,
    province: String,
    position: String,
    mobile: String,
    email: String,
    ): String

    updateRegiter(
        _id: ID
        title: String,
        name: String,
        surname: String,
        type_of_business: String,
        company: String,
        province: String,
        position: String,
        mobile: String,
        email: String,
    ): String

    deleteRegiter(
        _id: ID!
    ): String
}

`
export default regiter