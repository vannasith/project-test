import Regiter from '../../models/Regiter'

const regiter = {
    Query: {
        regiters: async (_parent: any, args: any) => {
            const regiters = await Regiter.find()
            return regiters
        }
    },
    Mutation: {
        createRegiter: async (_parent: any, args: any) => {
            try {
                const regiter = new Regiter({
                   
                    title: args.title,
                    name: args.name,
                    surname: args.surname,
                    type_of_business: args.type_of_business,
                    company: args.company,
                    province: args.province,
                    position: args.position,
                    mobile: args.mobile,
                    email: args.email,
                })
                await regiter.save()
                return 'created'
            } catch (e) {
                throw new Error(e)
            }
        },
        updateRegiter: async (_parent: any, args: any) => {
            try {
                await Regiter.findByIdAndUpdate(args._id, {
                    $set: {
                        title: args.title,
                    name: args.name,
                    surname: args.surname,
                    type_of_business: args.type_of_business,
                    company: args.company,
                    province: args.province,
                    position: args.position,
                    mobile: args.mobile,
                    email: args.email,
                        
                    }
                }, { new: true })
                return 'updated'
            } catch (e) {
                throw new Error(e)
            }
        },
        deleteRegiter: async (_parent: any, args: any) => {
            try{
                await Regiter.findByIdAndDelete(args._id)
                return 'deleted'

            } catch(e){
                throw new Error(e)
            }
        }
    }
}
export default regiter