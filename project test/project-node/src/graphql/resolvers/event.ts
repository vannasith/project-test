import Event from '../../models/Event'

const event = {
    Query: {
        events: async (_parent: any, args: any) => {
            const events = await Event.find()
            return events
        }
    },
    Mutation: {
        createEvent: async (_parent: any, args: any) => {
            try {
                const event = new Event({
                    title: args.title,
                    date: args.date,
                    description: args.description,
                    image: args.image
                })
                await event.save()
                return 'created'
            } catch (e) {
                throw new Error(e)
            }
        },
        updateEvent: async (_parent: any, args: any) => {
            try {
                await Event.findByIdAndUpdate(args._id, {
                    $set: {
                        title: args.title,
                        date: args.date,
                        description: args.description,
                        
                    }
                }, { new: true })
                return 'updated'
            } catch (e) {
                throw new Error(e)
            }
        },
        deleteEvent: async (_parent: any, args: any) => {
            try{
                await Event.findByIdAndDelete(args._id)
                return 'deleted'

            } catch(e){
                throw new Error(e)
            }
        }
    }
}
export default event