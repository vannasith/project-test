import { ApolloServer } from 'apollo-server-express'
import resolvers from './resolvers'
import typeDefs from './typeDefs'

const apollo = new ApolloServer({
    typeDefs,
    resolvers
})

export default apollo
