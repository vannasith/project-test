import mongoose from 'mongoose'
import dotenv from 'dotenv'

dotenv.config()
const dbName = process.env.DB_NAME || 'vstecs'
class Connect {
    uri: string
    constructor() {
        this.uri = `mongodb://127.0.0.1:27017/${dbName}`
    }
   async connect() {
        try {
            await mongoose.connect(this.uri, { //Connect to mongodb
                useNewUrlParser: true,
                useCreateIndex: true,
                useUnifiedTopology: true,
                useFindAndModify: false 
            })
            console.log('Connected to', this.uri)
        } catch(e) {
            throw new Error(e)
        }
    }
}
export default Connect