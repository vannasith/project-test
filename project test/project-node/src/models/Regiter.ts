import mongoose from 'mongoose'

const Schema = mongoose.Schema

const contactSchema = new Schema({
  
    title: String,
    name: String,
    surname: String,
    type_of_business: String,
    company: String,
    province: String,
    position: String,
    mobile: String,
    email: String,
    
}, {timestamps: true})

const Regiter = mongoose.model('regiter',contactSchema )

export default Regiter