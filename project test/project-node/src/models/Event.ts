import mongoose from 'mongoose'

const Schema = mongoose.Schema

const contactSchema = new Schema({
  
    title: String,
    date: String,
    description: String,
    image: String
}, {timestamps: true})

const Event = mongoose.model('events',contactSchema )

export default Event