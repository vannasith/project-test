import Vue from 'vue';
import VueRouter from 'vue-router';
import Home from '../views/Home.vue';

Vue.use(VueRouter);

const routes = [
  {
    path: '/',
    name: 'home',
    component: Home,
  },
  {
    path: '/event/:id',
    name: 'event',
    component: () => import('../views/Event.vue')
  },
  {
    path: '/admin',
    component: () => import('../views/admin/index.vue'),
    children: [
      {
        path: 'event',
        name: 'adminEvent',
        component: () => import('../views/admin/event.vue')
      }
    ]
  }
];

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes,
});

export default router;
