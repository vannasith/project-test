import gql from 'graphql-tag'

export const EVENTS = gql`

{
    events {
        _id
        title
        description
        image
        date
    }
}

`

export const CREATE_EVENT = gql`
mutation (
    $title: String
    $date: Date
    $description: String
    $image: String
) {
    createEvent(
        title: $title
        date: $date
        description:$description
        image:$image
    )
}

`

export const UPDATE_EVENT = gql`
mutation (
    $_id: ID
    $title: String
    $date: Date
    $description: String
 
) {
    updateEvent(
        _id: $_id
        title: $title
        date: $date
        description:$description
        
    )
}

`

export const DELETE_EVENT = gql`
mutation (
    $_id: ID!
) {
    deleteEvent(
        _id: $_id
    )
}
`