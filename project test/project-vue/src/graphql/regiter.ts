import gql from 'graphql-tag'

export const REGITERS = gql`

{
    regiters {
        _id
        title
        name
        surname
        type_of_business
        company
        province
        position
        mobile
        email
    }
}

`

export const CREATE_REGITER = gql`
mutation (
    $title: String
    $name: String
    $surname: String
    $type_of_business: String
    $company: String
    $province: String
    $position: String
    $mobile: String
    $email: String
) {
    createRegiter(
        title: $title
        name: $name
        surname: $surname
        type_of_business: $type_of_business
        company: $company
        province: $province
        position: $position
        mobile: $mobile
        email: $email
    )
}

`

export const UPDATE_REGITER = gql`
mutation (
    $_id: ID
    $title: String
    $name: String
    $surname: String
    $type_of_business: String
    $company: String
    $province: String
    $position: String
    $mobile: String
    $email: String
) {
    updateRegiter(
        _id: $_id
        title: $title
        name: $name
        surname: $surname
        type_of_business: $type_of_business
        company: $company
        province: $province
        position: $position
        mobile: $mobile
        email: $email
    )
}

`

export const DELETE_REGITER = gql`
mutation (
    $_id: ID!
) {
    deleteRegiter(
        _id: $_id
    )
}
`